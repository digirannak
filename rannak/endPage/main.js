const spans = document.querySelectorAll("span");

if (localStorage.roll != undefined){

    let roll = localStorage.roll;
    spans[0].innerHTML = roll;
    delete localStorage.roll;

    if (roll == 'Kaitseliitlase'){
        document.querySelector('h3').innerHTML = 'Soomusmasinate teekond:';
        spans[1].innerHTML = 'Asukoht: ' + localStorage.asula;
        //spans[2].innerHTML = 'Soomusmasinad möödusid sinust: ' + localStorage.kaugus ' km kauguselt.';
        spans[2].innerHTML = `Soomusmasinad möödusid sinust: ${localStorage.kaugus}  km kauguselt.`

        delete localStorage.asula;
        delete localStorage.kaugus;
    }

    else if (roll == 'Direktori'){
        document.querySelector('h3').innerHTML = 'Balti keti andmed:';
        spans[1].innerHTML = 'Asula: ' + localStorage.asula;
        spans[2].innerHTML = 'Kaugus: ' + localStorage.kaugus;
        spans[3].innerHTML = 'Balti keti pikkus: ' + localStorage.teekond;



        spans[4].innerHTML = "<br><br><h3><strong>Kõne</strong></h3><br>"
        spans[5].innerHTML = localStorage.kone;
        delete localStorage.kone;



        delete localStorage.asula;
        delete localStorage.kaugus;
        delete localStorage.teekond;
    }
}
