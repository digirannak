document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


let counter = 0;


function message(msg, color){
    messageBox = document.querySelector(".infoTxt");
    messageBox.innerHTML = msg;
    messageBox.style.color = color;
}





// Grid event added
function clickEventAdder(){
    for (let node of document.querySelectorAll("td")) {

        node.addEventListener("click", () => {

            if (!node.classList.contains("selected"))
                node.classList.add("selected");

            let curNode = node;
            for (let node of document.querySelectorAll("td")) {
                if (node != curNode && node.classList.contains("selected"))
                    node.classList.remove("selected");
            }

            // Bring cursor to input
            document.querySelector("input").focus();
            document.querySelector("input").select();

        });

    } // outerFor
} // FuncEnd
clickEventAdder();






function getInputValue() {
    // Selecting the input element and get its value 
    let inputVal = document.getElementById("inputId").value;

    if (inputVal == "")
        message("Palun vali täht.", "red");
    else
        kontrolliVastus(inputVal.toLowerCase());
}

let koodid = {"a": 25,
              "b": 24,
              "c": 23,
              "d": 22,
              "e": 21,
              "f": 20,
              "g": 19,
              "h": 18,
              "i": 17,
              "j": 16,
              "k": 15,
              "l": 14,
              "m": 13,
              "n": 12,
              "o": 11,
              "p": 10,
              "r": 9,
              "s": 8,
              "t": 7,
              "u": 6,
              "v": 5,
              "õ": 4,
              "ä": 3,
              "ö": 2,
              "ü": 1,
             };

function kontrolliVastus(vastus){

    let toCheck = document.querySelector(".selected");

    if (toCheck.classList.contains("done"))
        message("Juba õige!", "lime");

    else if (toCheck.innerHTML == "")
        message("Siin on tühik.", "lime");

    else if (koodid[vastus] == toCheck.innerHTML){
        message("Õige!", "lime");
        toCheck.innerHTML = vastus.toUpperCase();
        toCheck.classList.add("done");
        counter += 1;

        if (counter >= 90) {
            moveOnEnable();
            message("Tubli, sõnum dešifreeritud.<br/>Kogu Sõnum: TORNI RÜNNATI 4.30. VALVAME SIDEKESKUST 22. KORRUSEL. ELEKTER, RAADIOSIDE OLEMAS. LIFTID ÜLEVAL. OLUKORD KRIITILINE.", "lime");

            for (let node of document.querySelectorAll("td")) {
                if (node.classList.contains("selected"))
                    node.classList.remove("selected");
            }
        }

    }

    else
        message("Proovi uuesti.", "red");
    
}


function enterSubmit(){
    document.getElementById('inputId')
    .addEventListener('keyup', event => {
            if (event.code === 'Enter')
            {
                event.preventDefault();
                getInputValue();
            }
        });
}
enterSubmit()



// If Done

function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem("roll", "Kaitseliitlase");
    btn.onclick = () => location.href = "../../../endPage";
    // Proovi btn
	const subButton = document.querySelector(".subBtn");
	subButton.disabled = true;

}
