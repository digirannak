document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



const inputs = document.querySelectorAll('input');
let asulaVal;
let kaugusVal;

for (input of inputs){
    input.addEventListener('input', () => {
        getInputs()
    });
}


function getInputs(){
    let asula = document.querySelector('.asulaInput');
    let kaugus = document.querySelector('.kaugusInput');

    asulaVal = asula.value;
    kaugusVal = kaugus.value;

    if (asulaVal != '' && kaugusVal != '')
        allGood();
}



/// All Done ///



function allGood(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem("roll", "Kaitseliitlase");
    localStorage.setItem("asula", asulaVal);
    localStorage.setItem("kaugus", kaugusVal);
    btn.onclick = () => location.href = "../salakood";
}
