document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});




//  Draggable Items

const list_items = document.querySelectorAll(".list-item");
const lists = document.querySelectorAll(".list");

let draggedItem = null;

for (let i=0; i < list_items.length; i++){
	const item = list_items[i];

	/* Event Listeners */
	item.addEventListener("dragstart", e => {
		draggedItem = item;
		setTimeout(() => (item.style.display = "none"), 0);
	});

	item.addEventListener("dragend", e => {
		setTimeout(() => {
			draggedItem.style.display = "block";
			draggedItem = null}, 0);
		checkIfAllGood();
	});

}


//  Dropping Spots

for (let j=0; j < lists.length; j++){
	const list = lists[j];

	/* Event Listeners */

	list.addEventListener("dragenter",e => (e.preventDefault()));

	list.addEventListener("dragover",e => {
		e.preventDefault();
		list.style.backgroundColor = "rgba(0,0,0,0.9)";
		list.style.transform = "scale(1.3)";
	});


	list.addEventListener("dragleave",e => {
		list.style.backgroundColor = "rgba(0,0,0,0.3)";
		list.style.transform = "scale(1)";
	});


	list.addEventListener("drop",e => { 
		if (list.children.length == 0 || list.className.includes("startBasket"))
			list.append(draggedItem);
		list.style.backgroundColor = "rgba(0,0,0,0.3)";
		list.style.transform = "scale(1)";

	})
}








function checkIfAllGood(){
	let rightDone = [];

	for (let j=0; j < lists.length; j++){
		const list = lists[j];
		if (list.className.includes("startBasket"))
			continue;
		else if (list.children.length == 1){
			if (list.id == list.children[0].id){
				if (!rightDone.includes(list.id))
					rightDone.push(list.id);
				console.log(rightDone.length);
				console.log(rightDone);
			}
		}
	}  // For loop end

	if (rightDone.length == 6) {
        const btn = document.querySelector(".moveOn");
        btn.disabled = false;
        btn.onclick = () => location.href = "../soomusmasinad/index.html";
	}
}




// Smooth scroll stuff

// const ekraaniPikkus = window.innerHeight;
// 
// window.ondragover = function(e){
//     //console.log(e.clientY);
// 
//     if (e.clientY > (ekraaniPikkus - 300)){
//         window.scrollTo(0, 400); 
//     }
// 
//     else if (e.clientY < (300))
//         window.scrollTo(0, -400); 
// }



