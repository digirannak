document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


// To Know when to move on
changedArr = new Array();




// Loosung Font
let fontPick = document.querySelector("#fontPick");
fontPick.addEventListener("change", () => {

    let loosung = document.querySelector("#loosung");
    let val = fontPick.value; 
    loosung.style.fontFamily = val;

    if (!changedArr.includes("font"))
        changedArr.push("font");
    
    CheckIfAllGood();

});



// Loosung Size
let fontSize = document.querySelector("#fontSize");
fontSize.addEventListener("change", () => {


    let loosung = document.querySelector("#loosung");
    let val = fontSize.value; 
    let newSize = parseInt(val)/10; 
     

    if (val > 50)
        loosung.style.fontSize="5rem";
    
    else if (val < 10)
        loosung.style.fontSize="1rem";
    
    else
        loosung.style.fontSize = newSize+"rem";
    

    if (!changedArr.includes("size"))
        changedArr.push("size");
    
    CheckIfAllGood();

});




// Loosung Color
let fontColor = document.querySelector("#fontColor");
fontColor.addEventListener("change", () => {

    let loosung = document.querySelector("#loosung");
    let val = fontColor.value; 
    loosung.style.color = val;


    if (!changedArr.includes("color"))
        changedArr.push("color");
    
    CheckIfAllGood();

});




// Reset BTN
let resetBtn = document.querySelector("#resetBtn");
resetBtn.addEventListener("click", () => {

    let loosung = document.querySelector("#loosung");
    loosung.style.color= "#fff";
    loosung.style.fontSize= "2rem";
    loosung.style.fontFamily= "inherit";

    fontPick.value = "tavaline";
    fontColor.value = "#ffffff";
    fontSize.value = 20;

    let moveOnBtn = document.querySelector(".moveOn");
    moveOnBtn.disabled = true;

    loosung.innerHTML = "Muuda Loosung enda omaks!";
    changedArr = new Array();

});


document.querySelector("#loosung").addEventListener("input", () => {CheckIfAllGood()})



// BulletPoints eventListener
bulletBtns = document.querySelectorAll("#sundmus");
bulletBtns.forEach(element => {
   element.addEventListener('change', () => CheckIfAllGood());
});













// Other Funcs //

function CheckIfAllGood(){
    //let radioBtn = document.querySelector("input[name='sundmuseValik']:checked");
    let loosung = document.querySelector("#loosung");

    if (document.querySelector("input[name='sundmuseValik']:checked") != null &&
    changedArr.length >= 3 && loosung.innerHTML != "Muuda loosung enda omaks!"){
        moveOnEnable();
    }

    return false
}


// If Done
function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "../kone";
}

