document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



const inputs = document.querySelectorAll('input');
let asulaVal;
let kaugusVal;
let teekondVal;

for (input of inputs){
    input.addEventListener('input', () => {
        getInputs()
    });
}


function getInputs(){
    let teekond = document.querySelector('.teekondInput');
    let asula = document.querySelector('.asulaInput');
    let kaugus = document.querySelector('.kaugusInput');

    asulaVal = asula.value;
    kaugusVal = kaugus.value;
    teekondVal = teekond.value;

    if (asulaVal != '' && kaugusVal != '' && teekondVal != '')
        allGood();
}



/// All Done ///



function allGood(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem("asula", asulaVal);
    localStorage.setItem("kaugus", kaugusVal);
    localStorage.setItem("teekond", teekondVal);
    btn.onclick = () => location.href = "../loosung";
}
