document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


let map = {};
map["Moikka"] = "tere";
map["puhuda"] = "rääkida";
map["Pulma"] = "probleem";
map["ostoksille"] = "ostlema";
map["halba"] = "odavat";
map["pibod"] = "kootudsuusamütse";
map["Tpaitad"] = "t-särgid";
//map["kesähousut"] = "t-särgid";
map["puserod"] = "sametistdressipluusid"
map["takkid"] = "joped"
map["purukummi"] = "nätsu"
map["pulma"] = "probleemi";
map["kassi"] = "koti";
map["puhelin"] = "telefon";
map["kaikkiKunnossa"] = "kõikkorras";
map["KaikkiParhaat"] = "kõikehead";


let total = Object.keys(map).length;
rightOnes = new Array();


const words = document.querySelectorAll("span");

for (let w of words){
    w.addEventListener('DOMSubtreeModified', () => {

        sone = w.id;

        let x = w.innerText.replace(/\s/g, "");
        let ignoringSpaces = x.toLowerCase();

        if (ignoringSpaces == map[sone]) {

            let index = rightOnes.indexOf(sone);
            if (index == -1) {
                rightOnes.push(sone);
            }

            w.setAttribute("contenteditable", false);
            w.style.color = "lime";
        }


        if (rightOnes.length == total)
            moveOnEnable();
    });
}



// If All Else Done

function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "../pood";
}

