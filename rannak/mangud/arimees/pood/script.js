document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});




const cells = document.querySelectorAll("td");

for (let i=0;i < cells.length; i++){
    cells[i].onclick = function(){

        if (this.hasAttribute('data-clicked'))
            return;

        this.setAttribute('data-clicked', 'yes')
        this.setAttribute('data-text', this.innerHTML)

        let input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.value = this.innerHTML;
        input.style.width = this.offsetWidth - (this.clientLeft * 2) + 'px';
        input.style.height = this.offsetHeight - (this.clientTop * 2) + 'px';
        input.style.border = '0px';
        input.fontFamily = 'inherit';
        input.fontSize = 'inherit';
        input.textAlign = 'inherit';
        input.style.backgroundColor = 'LightGoldenRodYellow';


        // On offclick
        input.onblur = function(){
            let td = input.parentElement;
            let origText = input.parentElement.getAttribute('data-text');
            let curText = this.value;

            sums = document.querySelectorAll(".summa");
            console.log(sums);

            if (origText != curText){
                td.removeAttribute('data-clicked');
                td.removeAttribute('data-text');
                td.innerHTML = curText;
                td.style.cssText = 'padding: .5rem .5rem';


                totalSumma = 0; 
                sums.forEach(el => {
                    totalSumma += Number(el.innerHTML);
                });
                // Can move on?
                for (cell of cells){
                    if (totalSumma >= 11200 && totalSumma <= 12000 ) 
                        moveOnEnable();
                    else if (cell.textContent == '')
                            return;
                }
                moveOnEnable();
            }
            
            else {
                td.removeAttribute('data-clicked');
                td.removeAttribute('data-text');
                td.innerHTML = origText;
                td.style.cssText = 'padding: .5rem .5rem';

                sums.forEach(el => {
                    console.log(el); 
                });
            }


        } // onblur func end


        input.onkeypress = function (){
            if (event.keyCode == 13)
                this.onblur();
        }

        this.innerHTML = '';
        this.style.cssText = 'padding: 0px';
        this.append(input);
        this.firstElementChild.select();
        

    } // OnClick Loop
} //ForLoop



// If All Done

function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "../muusika";
}

