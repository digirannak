document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


allPohjendatud = false;
allSelected = false;


// EventListenres
const selectors = document.querySelectorAll('select');
for(el of selectors){
    el.addEventListener('change', () => {

        let selected = new Array();
        for (el of selectors){
            if (!selected.includes(el.value) && el.value != 'Vali')
                selected.push(el.value);
        }

        if (selected.length == 6)
            allSelected = true;
        else
            allSelected = false;
        
        if (allPohjendatud && allSelected)
            moveOnEnable();
    });
}



const pohjendused = document.querySelectorAll('textarea');
for(el of pohjendused){
    el.addEventListener('input', () => {

        let pohjendatud = new Array();
        for (el of pohjendused){
            if (!pohjendatud.includes(el) && el.value != '')
                pohjendatud.push(el);
        }

        if (pohjendatud.length == 6)
            allPohjendatud = true;
        else
            allPohjendatud = false;

        if (allPohjendatud && allSelected)
            moveOnEnable();
        
    });
}








// If All Done

function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem("roll", "Ärimehe");
    btn.onclick = () => location.href = "../../../endPage";
}

