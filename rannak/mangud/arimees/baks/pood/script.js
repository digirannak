document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



const kurss = 15.6;
doneCounter = 0;


const kontrollBtns = document.querySelectorAll(".kontrolliKaarti");
for (let nupp of kontrollBtns){
    nupp.addEventListener("click", (e) => {
        let rublaHind = e.target.previousElementSibling.children[0].children[1].innerHTML;
        let pakkumine = e.target.previousElementSibling.children[1].children[1].value;

        if (pakkumine*kurss == rublaHind){

            //Change color
            e.target.offsetParent.style.background = "#288654";
            e.target.previousElementSibling.children[0].style.borderRight = ".5rem solid #288654";
            e.target.previousElementSibling.children[1].style.borderLeft = ".5rem solid #288654";

            //Change Element

            (e.target.previousElementSibling.children[1].children[1]).remove();
            const newEl = document.createElement("span");
            newEl.classList.add("symbol");
            newEl.innerHTML = pakkumine + " M";
            e.target.previousElementSibling.children[1].appendChild(newEl);

            e.target.remove(); 
            doneCounter++;


            // InfoTxt
            let infoTxt = document.querySelector(".infoTxt")
            infoTxt.innerHTML = "";

            if (doneCounter >= 6){
                enableAllGood();
            }
        }


        else{
            // Inputvalue Reset
            e.target.previousElementSibling.children[1].children[1].value = "";

            // InfoTxt
            let infoTxt = document.querySelector(".infoTxt")
            infoTxt.style.color = "red";
            infoTxt.innerHTML = "Proovi Uuesti!";

        }



    })
}



function enableAllGood(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "";
}
