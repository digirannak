document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



/*Muutused Stuff*/

// Särgi color

let colorFill = document.querySelector("#colorFiller");
let colorStroke = document.querySelector("#colorStroke");

colorFill.addEventListener("input", () => {
    let color = colorFill.value;
    document.querySelector("#sark").style.fill = color;

    if (!stuffDone.includes("sarkColor"))
        stuffDone.push("sarkColor");
})

colorStroke.addEventListener("input", () => {
    let color = colorStroke.value;
    document.querySelector("#sark").style.stroke = color;

    if (!stuffDone.includes("sarkColor"))
        stuffDone.push("sarkColor");
})


// Img upload

const image_input = document.querySelector("#image_input");
let uploadedImg = "";

image_input.addEventListener("change", () => {
    const reader = new FileReader();
    reader.readAsDataURL(image_input.files[0])
    reader.addEventListener("load", () => {
        uploadedImg = reader.result;
        document.querySelector(".item").style.backgroundImage = `url(${uploadedImg})`
    });

    if (!stuffDone.includes("newImg"))
        stuffDone.push("newImg");
})


/* Teksti Teema */

// Uus Tekst

function nextText() {
    document.querySelector("#sargiText").value = "";

    let curText = document.querySelector(".sarkTekst")
    let newText = curText.cloneNode(true);
    curText.classList.remove("sarkTekst")

    //Reset Stuff
    let angle=0;
    let sarkFontSize = 20;
    
    let gi6 = document.querySelector("g")
    newText.classList.add("sarkTekst")
    newText.id = 'sarkTekst';
    newText.innerHTML = "Uus Tekst";

    // Move a little, so that new txt isnt exatly on top of old.
    let newYVal = newText.y.baseVal[0].value;
    if (newYVal < -150)
        newText.y.baseVal[0].value += 50;
    else
        newText.y.baseVal[0].value -= 50;
    //console.log(newText.y.baseVal[0].value);

    gi6.appendChild(newText);
}



// Input

function chgTxt() {

    let tekstVali = document.querySelector("#sargiText");
    tekstVali.addEventListener("keyup", () => {
        let sarkTekst = document.querySelector(".sarkTekst");
        sarkTekst.innerHTML = tekstVali.value;

        if (!stuffDone.includes("txt"))
            stuffDone.push("txt");
    });
}

chgTxt()


/*
function getInputValue() {
    // Selecting the input element and get its value 

    //Btn
    let inputVal = document.getElementById("sargiText").value;
    //Txt
    let sarkTekst = document.querySelector(".sarkTekst");
    sarkTekst.innerHTML = inputVal;

    if (!stuffDone.includes("newTxt"))
        stuffDone.push("newTxt");
}

function enterSubmit(){
    document.getElementById('sargiText')
    .addEventListener('keyup', event => {
            if (event.code === 'Enter')
            {
                event.preventDefault();
                getInputValue();
            }
        });
}
enterSubmit()
*/


// Color

let colorText = document.querySelector("#colorText");
colorText.addEventListener("input", () => {
    let color = colorText.value;
    document.querySelector(".sarkTekst").style.fill = color;

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
})

// Position
const ules = document.querySelector(".ules");
const alla = document.querySelector(".alla");
const vasak = document.querySelector(".vasak");
const parem = document.querySelector(".parem");

ules.addEventListener("click" , () => {
    let tmpX = document.querySelector(".sarkTekst").x;
    let tmpY = document.querySelector(".sarkTekst").y;

    tmpY.baseVal[0].value-=10;
    
    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});

alla.addEventListener("click" , () => {
    let tmpX = document.querySelector(".sarkTekst").x;
    let tmpY = document.querySelector(".sarkTekst").y;

    tmpY.baseVal[0].value+=10;

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});

vasak.addEventListener("click", () => {
    let tmpX = document.querySelector(".sarkTekst").x;
    let tmpY = document.querySelector(".sarkTekst").y;

    tmpX.baseVal[0].value-=10;

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});

parem.addEventListener("click", () => {
    let tmpX = document.querySelector(".sarkTekst").x;
    let tmpY = document.querySelector(".sarkTekst").y;

    tmpX.baseVal[0].value+=10;

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});


// Vertical Pos
const vasakule = document.querySelector(".vertVasak");
const paremale = document.querySelector(".vertParem");
let angle=0;

vasakule.addEventListener("click" , () => {
    let testing = document.querySelector(".sarkTekst");

    angle -=5
    testing.setAttribute("transform", "rotate("+angle+")");

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});

paremale.addEventListener("click" , () => {
    let testing = document.querySelector(".sarkTekst");

    angle +=5
    testing.setAttribute("transform", "rotate("+angle+")");

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});



// FontZise

const plus = document.querySelector(".plus");
const minus = document.querySelector(".minus");
let sarkFontSize = 20;

plus.addEventListener("click" , () => {
    let sarkForText = document.querySelector(".sarkTekst");

    sarkFontSize +=2;
    sarkForText.setAttributeNS(null,"font-size",""+sarkFontSize+"");

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});

minus.addEventListener("click" , () => {
    let sarkForText = document.querySelector(".sarkTekst");

    sarkFontSize-=2;
    sarkForText.setAttributeNS(null,"font-size",""+sarkFontSize+"");

    if (!stuffDone.includes("txt"))
        stuffDone.push("txt");
});













/* Rezisable Image Stuff */

const el = document.querySelector(".item");

el.addEventListener("mousedown", mousedown);

function mousedown(e) {
  window.addEventListener("mousemove", mousemove);
  window.addEventListener("mouseup", mouseup);

  let prevX = e.clientX;
  let prevY = e.clientY;

  function mousemove(e) {
    if (!isResizing) {


        const rect = el.getBoundingClientRect();

        let item = document.querySelector(".item");
        item.style.pointerEvents = "none";
        //let posers = document.elementFromPoint(e.x, e.y).id;

        // New Stuff
        let posers = document.elementFromPoint(rect.left, rect.top).id;
        let posers2 = document.elementFromPoint(rect.right, rect.top).id;
        let posers3 = document.elementFromPoint(rect.right, rect.bottom).id;
        let posers4 = document.elementFromPoint(rect.left, rect.bottom).id;
        // /// /////


        item.style.pointerEvents = "";


        //if ( posers != "sark" || posers2 != "sark" || posers3 != "sark" || posers4 != "sark") {
        if ( !(posers == "sark" || posers == "sarkTekst") || !(posers2 == "sark" || posers2 == "sarkTekst") ||
             !(posers3 == "sark" || posers3 == "sarkTekst") || !(posers4 == "sark" || posers4 == "sarkTekst")) {

            let newX = prevX - e.clientX;
            let newY = prevY - e.clientY;

            // Pic back to Center
            let brr = document.querySelector("#sark").getBoundingClientRect();
            el.style.left = brr.left + (brr.right - brr.left)/2 - (rect.right - rect.left)/2 + "px";
            el.style.top  = brr.top +  (brr.bottom - brr.top)/2 - (rect.bottom - rect.top)/2 + "px";
            //el.style.top  = brr.bottom - 2*(rect.bottom - rect.top) + "px";


            prevX = e.clientX;
            prevY = e.clientY;
            mouseup()
        }

        else {

            let newX = prevX - e.clientX;
            let newY = prevY - e.clientY;

            const rect = el.getBoundingClientRect();

            el.style.left = rect.left - newX + "px";
            el.style.top = rect.top - newY + "px";

            prevX = e.clientX;
            prevY = e.clientY;
        }
    }
  }

  function mouseup() {
    window.removeEventListener("mousemove", mousemove);
    window.removeEventListener("mouseup", mouseup);
  }

}






const resizers = document.querySelectorAll(".resizer");
let currentResizer;
let isResizing = false;


for (let resizer of resizers) {
  resizer.addEventListener("mousedown", mousedown);

  function mousedown(e) {
    currentResizer = e.target;
    isResizing = true;

    let prevX = e.clientX;
    let prevY = e.clientY;

    window.addEventListener("mousemove", mousemove);
    window.addEventListener("mouseup", mouseup);

    function mousemove(e) {

        const rect = el.getBoundingClientRect();



        let item = document.querySelector(".item");
        item.style.pointerEvents = "none";
        let posers = document.elementFromPoint(rect.left, rect.top).id;
        let posers2 = document.elementFromPoint(rect.right, rect.top).id;
        let posers3 = document.elementFromPoint(rect.right, rect.bottom).id;
        let posers4 = document.elementFromPoint(rect.left, rect.bottom).id;
        item.style.pointerEvents = "";



        if ( !(posers != "sark" || posers2 != "sark" || posers3 != "sark" || posers4 != "sark")){

            
            if (currentResizer.classList.contains("se")) {
                el.style.width = rect.width - (prevX - e.clientX) + "px";
                el.style.height = rect.height - (prevY - e.clientY) + "px";
            }
            
            else if (currentResizer.classList.contains("sw")) {
                el.style.width = rect.width + (prevX - e.clientX) + "px";
                el.style.height = rect.height - (prevY - e.clientY) + "px";
                el.style.left = rect.left - (prevX - e.clientX) + "px";
            }

            else if (currentResizer.classList.contains("ne")) {
                el.style.width = rect.width - (prevX - e.clientX) + "px";
                el.style.height = rect.height + (prevY - e.clientY) + "px";
                el.style.top = rect.top - (prevY - e.clientY) + "px";
            }

            else {
                el.style.width = rect.width + (prevX - e.clientX) + "px";
                el.style.height = rect.height + (prevY - e.clientY) + "px";
                el.style.top = rect.top - (prevY - e.clientY) + "px";
                el.style.left = rect.left - (prevX - e.clientX) + "px";
            }

        }

        // If resizing out of boundaries
        else{

            if (currentResizer.classList.contains("se") || currentResizer.classList.contains("sw")) {
                el.style.width = rect.width - 15 + "px";
                el.style.height = rect.height - 15 + "px";
                mouseup();
            }
            
            if (currentResizer.classList.contains("ne") || currentResizer.classList.contains("nw")) {
                el.style.width = rect.width - 15 + "px";
                el.style.height = rect.height - 15 + "px";
                mouseup();
            }

        }
            prevX = e.clientX;
            prevY = e.clientY;

    } // Function mousemove end

    function mouseup() {
      window.removeEventListener("mousemove", mousemove);
      window.removeEventListener("mouseup", mouseup);
      isResizing = false;
    }

  } // Resizing mousedown end

}


// Seting up resize Pic Pos
let brr = document.querySelector("#sark").getBoundingClientRect();
let it = document.querySelector(".item").getBoundingClientRect();
let Boop = document.querySelector(".item");
//it.style.left = brr.left + 200;
Boop.style.left = brr.left + (brr.right - brr.left)/2 - (it.right - it.left)/2 + "px";
Boop.style.top  = brr.top +  (brr.bottom - brr.top)/2 - (it.bottom - it.top)/2 + "px";






/* Move on Button*/
function message(msg, color){
    messageBox = document.querySelector(".messageBox");
    messageBox.innerHTML = msg;
    messageBox.style.color = color;
}


let stuffDone = new Array();
const chkBtn = document.querySelector(".checkMoveOn");

chkBtn.addEventListener("click", (e) => {
    if (stuffDone.length >= 3){
        message("Korras! Soovi korral tee screenshot (Win+Shift+S/Cmd+Shift+4)", "#4BB543");
        moveOnEnable();
    }
    else
        message("valikuid kasutatud " + stuffDone.length + "/" + "3.", "Red");
});


function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "../utlused";

    let tst = document.querySelectorAll(".resizer");
    for (elinjo of tst){
        elinjo.style.height = 0;
        elinjo.style.width = 0;
    }

}





// Pic back to Center
function imgToCenter(){
    let brr =  document.querySelector("#sark").getBoundingClientRect();
    let rect = document.querySelector(".item").getBoundingClientRect();
    el.style.left = brr.left + (brr.right - brr.left)/2 - (rect.right - rect.left)/2 + "px";
    el.style.top  = brr.top +  (brr.bottom - brr.top)/2 - (rect.bottom - rect.top)/2 + "px";
}






// Reset Everything

const nulliBtn = document.querySelector(".nulliBtn");
nulliBtn.addEventListener("click", () => {

    stuffDone = new Array();

    //SarkColor
    document.querySelector("#sark").style.fill = "white";

    document.querySelector("#sark").style.stroke = "black";



    // Tekstid
    document.querySelector("#sargiText").value = "";

    curText = document.querySelector(".sarkTekst");
    newText = curText.cloneNode(true);
    curText.classList.remove("sarkTekst");

    let koikTekstid = document.querySelectorAll(".doneTekst");

    for (let i = 0; i < koikTekstid.length; i++) {
        koikTekstid[i].innerHTML = "";
    }

    //Reset Stuff
    angle=0;
    sarkFontSize = 20;
    
    let brro = document.querySelector("g")
    newText.classList.add("sarkTekst")
    newText.innerHTML = "Tekst";

    brro.appendChild(newText);
    document.querySelector(".sarkTekst").style.fill = "black";


    // Pic back to Center
    imgToCenter();


    // Moving IMg Corners
    let tst = document.querySelectorAll(".resizer");
    for (elinjo of tst){
        elinjo.style.height = "7px";
        elinjo.style.width = "7px";
    }

})
