document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


// Vars
curZindex = 1;



function randomLetters(){
    for (let node of document.querySelectorAll("td")) {
        if (node.textContent != "") continue;
        let charcode = Math.round(65 + Math.random() * 25)
        node.textContent = String.fromCharCode(charcode)
    }
}
randomLetters();

function clickEventAdder(){
    for (let node of document.querySelectorAll("td")) {
        node.addEventListener("click", () => {
            if (!node.classList.contains("selected")){
                node.classList.add("selected");
            }
            else{
                node.classList.remove("selected");
            }

        });
    }
}
clickEventAdder();



function message(msg, color){
    messageBox = document.querySelector(".messageBox");
    messageBox.innerHTML = msg;
    messageBox.style.color = color;
}



// Main stuff

const vastused = ["valuuta", "tsaika", "redis", "plaanimajandus",
                  "rubla", "letialuneKaup", "talongid", "maantee",
                  "piiritsoon", "defitsiit"];

const horisontaalis = ["letialuneKaup", "talongid", "maantee",
                     "piiritsoon", "defitsiit" ];

let õiged = new Array();
let õigedCounter = 0;

function proovi(){

    let dones = new Array();

    for (let klass of vastused){

        const ruudud = document.querySelectorAll("."+klass);
        const allSelected = document.querySelectorAll(".selected").length;
        let kokku = ruudud.length;
        let counter = 0;
        

        for (let ruut of ruudud){
            if (ruut.classList.contains("selected"))
                counter++;
        }

        if (counter == kokku && counter == allSelected){
            const selectd = document.querySelectorAll(".selected");
            let hetkeKlass;

            for (let ruut of ruudud){
                ruut.classList.remove("selected");
                //ruut.classList.add("done");

                dones.push(ruut);

                if (ruut.classList.length == 1)
                    hetkeKlass = ruut.classList[0];
            }

            //console.log("hetkeKlass: ",hetkeKlass);

            for (let el of dones){
                if (horisontaalis.includes(hetkeKlass)){
                    el.classList.add("doneH");
                }
                else{
                    el.classList.add("doneV");
                }
            }

            //console.log(document.querySelector(".V"+hetkeKlass).classList);
            document.querySelector(".V"+hetkeKlass).classList.add("done");
        
            if (!õiged.includes(hetkeKlass)) {
                õiged.push(hetkeKlass);
                õigedCounter++;
                //console.log(õigedCounter);
            }
            if (õigedCounter >= 10)
                moveOnEnable();


            message("Õige", "green");
            return;
        }

    } //Outerloop
    const selectd = document.querySelectorAll(".selected");
    for (let sel of selectd)
        sel.classList.remove("selected");
    message("Proovi uuesti", "red");
}



// Button func
function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem('roll', "Partorgi");
    btn.onclick = () => location.href = "../../../endPage";

    // submit btn
	const subButton = document.querySelector(".subBtn");
	subButton.disabled = true;

	counter = 0;
}

