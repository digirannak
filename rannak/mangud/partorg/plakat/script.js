document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



let ajastudCounter = 0
let addedTexts = new Array();


// New Text Adding
let newTextBtns = document.querySelectorAll(".muuda");

newTextBtns.forEach(btn => {
    btn.addEventListener("click" , e => {

        console.log("clicked");

        for (el of e.target.parentElement.parentElement.childNodes[1].childNodes) {
            if (el.classList != undefined && el.classList.contains("addedText")){
                el.classList.remove("addedText");
            }
        }


        let inputText = e.target.parentElement.childNodes[5].value;


        let mySpan = document.createElement('span');
        mySpan.classList.add('addedText');
        mySpan.classList.add('oldAddedText');
        mySpan.innerHTML = inputText;



        let parentEl= e.target.parentElement.parentElement.childNodes[1];
        parentEl.appendChild(mySpan);

        mySpan.style.top = "50%";
        mySpan.style.left = "35%";


        e.target.parentElement.childNodes[5].value = '';
        
        if (!addedTexts.includes(parentEl.classList[1]))
            addedTexts.push(parentEl.classList[1]);




        // Check If Done
        if (addedTexts.length >= 4)
            checkIfAllGood();

            
    });
})




// Start Text Edit 

let selectors = document.querySelectorAll("select");
selectors.forEach(el => {
    el.addEventListener("change", () => {
        if(el.className == el.value)
            enableEditing(el);
    });
});



function enableEditing(el){
    ajastudCounter += 1;

    el.style.backgroundColor = 'lime';
    el.style.border = 'none';
    el.style.borderRadius = '7px';
    el.disabled = true;

    
    let imgDiv = el.parentElement.parentElement.childNodes[1];
    let blankImgLoc = `img/${el.id}Blank.png`;

    imgDiv.style.background = "url(" + blankImgLoc + ") no-repeat center";    
    imgDiv.style.backgroundSize = "100% 100%";

    let parent = el.parentElement.id;
    
    // Enable buttons
    let btns = document.querySelectorAll(`#${parent} button`);
    btns.forEach(btn => {
        btn.disabled = false; 
    });

    
    // Enable inputs
    let inputs = document.querySelectorAll(`#${parent} input`);
    inputs.forEach(input => {
        input.disabled = false; 
    });


}



// Reset Button
let resetBtns = document.querySelectorAll(".reset");
resetBtns.forEach(btn => {
    btn.addEventListener("click", () => {

        let imgDiv = btn.parentElement.parentElement.childNodes[1];
        imgDiv.innerHTML = '';

        addedTexts = new Array();
        ajastudCounter = 0;

       
    });
});



// Color Changer

let colorPickers = document.querySelectorAll("#color");
colorPickers.forEach(el => {

    el.addEventListener("change", () => {

        for (text of el.parentElement.parentElement.childNodes[1].childNodes){
            if (text.classList != undefined && text.classList.contains("addedText")){


                let pickedColor = el.value;
                text.style.color = pickedColor;


            }
        }
    });
});





// fontSize 

let fontIncs = document.querySelectorAll(".pluss");
fontIncs.forEach(el => {
    el.addEventListener("click", (e) => {
        
        for (text of el.parentElement.parentElement.parentElement.childNodes[1].childNodes){
            if (text.classList != undefined && text.classList.contains("addedText")){


                var font = window.getComputedStyle(text, null).getPropertyValue('font-size');
                var fontSize = parseFloat(font); 
                
                text.style.fontSize = (fontSize + 1) + 'px';


            }
        }
    });
});



let fontDecs = document.querySelectorAll(".miinus");
fontDecs.forEach(el => {
    el.addEventListener("click", (e) => {
        
        for (text of el.parentElement.parentElement.parentElement.childNodes[1].childNodes){
            if (text.classList != undefined && text.classList.contains("addedText")){


                var font = window.getComputedStyle(text, null).getPropertyValue('font-size');
                var fontSize = parseFloat(font); 
                
                text.style.fontSize = (fontSize - 1) + 'px';


            }
        }
    });
});












// Position
const ules  = document.querySelectorAll(".ules");
const alla  = document.querySelectorAll(".alla");
const vasak = document.querySelectorAll(".vasak");
const parem = document.querySelectorAll(".parem");



ules.forEach(el => {
    el.addEventListener("click" , e => {
        //let theText = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes[1];
        let texts = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes;
        
        let theText;
        for (el of texts){
            if (el.classList != undefined && el.classList.contains("addedText"))
                theText = el;
        }
        

        let curTop = theText.style["top"];
        curTop = curTop.substring(0, curTop.length-1)
        
        if (Number(curTop) >= 5)
            theText.style["top"] = Number(curTop)-5+"%";
    });
})



alla.forEach(el => {
    el.addEventListener("click" , e => {
        //let theText = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes[1];
        let texts = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes;
        
        let theText;
        for (el of texts){
            if (el.classList != undefined && el.classList.contains("addedText"))
                theText = el;
        }


        let curTop = theText.style["top"];
        curTop = curTop.substring(0, curTop.length-1)
        
        if (Number(curTop) <= 85)
            theText.style["top"] = Number(curTop)+5+"%";
    });
})



vasak.forEach(el => {
    el.addEventListener("click", e => {
        //let theText = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes[1];

        let texts = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes;
        
        let theText;
        for (el of texts){
            if (el.classList != undefined && el.classList.contains("addedText"))
                theText = el;
        }



        let curLeft = theText.style["left"];
        curLeft = curLeft.substring(0, curLeft.length-1)
        
        if (Number(curLeft) >= 5)
            theText.style["left"] = Number(curLeft)-5+"%";
    });
})





parem.forEach(el => {
    el.addEventListener("click", e => {
        //let theText = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes[1];

        let texts = e.target.parentElement.parentElement.parentElement.childNodes[1].childNodes;
        
        let theText;
        for (el of texts){
            if (el.classList != undefined && el.classList.contains("addedText"))
                theText = el;
        }



        let curLeft = theText.style["left"];
        curLeft = curLeft.substring(0, curLeft.length-1)


        // Get the width of text itself and add the "left".
        let offWi = theText.offsetWidth;
        let parentWi = theText.parentElement.clientWidth;
        let textWi = (offWi/parentWi)*100;


        if (Number(curLeft)+textWi <= 95)
            theText.style["left"] = Number(curLeft)+5+"%";


    });
})





function checkIfAllGood(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
        btn.onclick = () => location.href = "../kuulsused";
}
