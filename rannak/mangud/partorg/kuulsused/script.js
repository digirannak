document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});



const list_items = document.querySelectorAll(".list-item");
const lists = document.querySelectorAll(".list");

let draggedItem = null;


// Draggable Items

for (let i=0; i < list_items.length; i++){
	const item = list_items[i];

	/* Event Listeners */
	item.addEventListener("dragstart", e => {
		draggedItem = item;
		setTimeout(() => (item.style.display = "none"), 0);

	});


	item.addEventListener("dragend", e => {

		setTimeout(() => {
			draggedItem.style.display = "block";
			draggedItem = null}, 0);
		checkIfAllGood();
	});


}




// Dropping spots

for (let j=0; j < lists.length; j++){
	const list = lists[j];

	/* Event Listeners */

	list.addEventListener("dragenter",e => (e.preventDefault()));

		list.addEventListener("dragover",e => {
			e.preventDefault();
			list.style.backgroundColor = "rgba(0,0,0,0.9)";
		});


		list.addEventListener("dragleave",e => {
			list.style.backgroundColor = "rgba(0,0,0,0.3)";
		});


		list.addEventListener("drop",e => { 
			if (list.children.length == 0 || list.className.includes("startBasket"))
				list.append(draggedItem);
			list.style.backgroundColor = "rgba(0,0,0,0.3)";
		})
}



// Smooth scroll stuff

// const ekraaniPikkus = window.innerHeight;
// let d;
// if (ekraaniPikkus > 800)
//     d = ekraaniPikkus / 8;
// else
//     d = ekraaniPikkus / 4;
// 
// console.log(ekraaniPikkus);
// console.log(document.body.scrollHeight);
// const maxScroll = document.body.scrollHeight;
// 
// window.ondragover = function(e){
//     console.log("Pos: ",e.clientY);
//     console.log(d);
//     console.log("X: ", e.y);
//     //console.log(e);
// 
//     if (e.clientY > (ekraaniPikkus - d)){
//         console.log("Should scroll down");
//         if (maxScroll > e.y+d)
//             window.scrollTo(0, e.y + d);
//         else
//             window.scrollTo(0, maxScroll);
//     }
//     else if (e.clientY < (d))
//         window.scrollTo(0, e.y - d);
// }





function checkIfAllGood(){
	let rightDone = [];

	for (let j=0; j < lists.length; j++){
		const list = lists[j];
		if (list.className.includes("startBasket"))
			continue;
		else if (list.children.length == 1){
			if (list.id == list.children[0].id){
				if (!rightDone.includes(list.id))
					rightDone.push(list.id);
			}
		}
	}  // For loop end

	if (rightDone.length == 6) {
        const btn = document.querySelector(".moveOn");
        btn.disabled = false;
        btn.onclick = () => location.href = "../sonaragastik";
	}
}



/* The Scrolling on thing while dragging */
// window.scrollTo(0, 500); 
