document.addEventListener('DOMContentLoaded', () => {
    let btn = document.querySelector('.modalButton');
    btn.click();
});


let counter = 0;
let õiged = new Array();


function message(msg, color){
    messageBox = document.querySelector(".messageBox");
    messageBox.innerHTML = msg;
    messageBox.style.color = color;
}


function õigeVastus(nr, vastus){
    message("Õige!", "green");

    let tähed = document.querySelectorAll("."+vastus);
    for (let i = 0; i < tähed.length; i++) {
            tähed[i].childNodes[0].style.display = "inline";
    }

    if (!õiged.includes(nr)) {
        õiged.push(nr);
        counter++;
    }
    if (counter >= 12)
        moveOnEnable();
}


function valeVastus(tähed, nr, vastus){
    message("Vale pakumine, proovi uuesti", "red");

    for (let i = 0; i < tähed.length; i++) {
            tähed[i].childNodes[0].style.display = "none";
    }

    if (õiged.includes(nr)) {
        delete õiged[õiged.indexOf(nr)];
        counter--;
    }

}

function kontrolliVastus(nr, vastus){
    nr += ''; // dont know, andis errorit

    switch (nr) {
    case "1": 
        if (vastus == "kurk") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".kurk");
            valeVastus(tähed, vastus, nr);   
        }
        break;
        

    case "2": 
        if (vastus == "majonees") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".majonees");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    case "3": 
        if (vastus == "porgand") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".porgand");
            valeVastus(tähed, vastus, nr);   
        }
        break;
        
        
    case "4": 
        if (vastus == "vorst") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".vorst");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    case "5": 
        if (vastus == "õun") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".õun");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    case "6": 
        if (vastus == "sool") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".sool");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    case "7": 
        if (vastus == "pipar") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".pipar");
            valeVastus(tähed, vastus, nr);   
        }
        break;

    case "8": 
        if (vastus == "hernes") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".hernes");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    case "9": 
        if (vastus == "muna") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".muna");
            valeVastus(tähed, vastus, nr);   
        }
        break;
        
    case "10": 
        if (vastus == "sibul") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".sibul");
            valeVastus(tähed, vastus, nr);   
        }
        break;
        
    case "11": 
        if (vastus == "hapukoor") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".hapukoor");
            valeVastus(tähed, vastus, nr);   
        }
        break;

    case "12": 
        if (vastus == "kartul") õigeVastus(nr, vastus);
        else{
            let tähed = document.querySelectorAll(".kartul");
            valeVastus(tähed, vastus, nr);   
        }
        break;


    default: message("Proovi uuesti", "red");
    }
    
}



function getInputValue() {
    // Selecting the input element and get its value 
    let inputVal = document.getElementById("inputId").value;
    let inputNr = document.getElementById("inputNr").value;

    if (inputNr == "" || inputVal == ""){
        message("Palun täida mõlemad väljad", "red");
    }
    else{
        kontrolliVastus(inputNr, inputVal.toLowerCase());
    }

    // Clear input value
    let inputs = document.querySelectorAll("input");
    for (let ipt of inputs)
        ipt.value = "";
}




// Button funcs

function enterSubmit(){
    document.getElementById('inputId')
    .addEventListener('keyup', event => {
            if (event.code === 'Enter')
            {
                event.preventDefault();
                getInputValue();
            }
        });
}
enterSubmit()



// If All Else Done


function moveOnEnable(){
    message("<strong>Tubli, täitsid ristsõna. Siin on kartulisalati retsept</strong>", "green");

    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    localStorage.setItem("roll", "Perenaise");
    btn.onclick = () => location.href = "../../../endPage";

    // Retsept
    const middlePart = document.querySelector("table");
    middlePart.parentNode.removeChild(middlePart);
    
    const leftPart = document.querySelector(".left");
    let eller = document.createElement("span");
    
    eller.innerHTML = "- Keeda koorimata aedviljad eelmisel päeval pehmeks. Lase täielikult jahtuda.</br></br> - Salati tegemiseks koori keedetud kartulid ja porgandid. Haki aedviljad (kartul, porgand, kurk) ja vorst väikesteks kuubikuteks. Haki kahvliga jahtunud munad. Kastme jaoks sega hapukoor ja majonees omavahel. Lisa soola ja pipart. Kalla kaste salatiainetele ning sega õrnalt läbi. </br></br> - Enne serveerimist lase salatil 2-4 tundi maitsestuda.</br></br> - Salatile võib lisada ka hakitud õunakuubikuid ning konservherneid."
    eller.classList.add("retsept");
    
    leftPart.parentNode.insertBefore(eller, leftPart.nextSibling);


    // Remove left and right part
    leftPart.classList.remove("text-white");
    leftPart.style.color = "#43464b";

    const rightPart = document.querySelector(".right");
    rightPart.classList.remove("text-white");
    rightPart.style.color = "#43464b";




    // submit btn
	const subButton = document.querySelector(".subBtn");
	subButton.disabled = true;

	counter = 0;
}

