// ******************** Zoom Stuff **************************** //

const svg = document.querySelector("svg");

const getTransformParameters = (element) => {
  const transform = element.style.transform;
  let scale = 1, x = 0, y = 0;

  if (transform.includes("scale"))
    scale = parseFloat(transform.slice(transform.indexOf("scale") + 6));
  if (transform.includes("translateX"))
    x = parseInt(transform.slice(transform.indexOf("translateX") + 11));
  if (transform.includes("translateY"))
    y = parseInt(transform.slice(transform.indexOf("translateY") + 11));

  return { scale, x, y };
};


const getTransformString = (scale, x, y) =>
  "scale(" + scale + ") " + "translateX(" + x + "%) translateY(" + y + "%)";


// Pan and Zoom funcs

const pan = (direction) => {
  const { scale, x, y } = getTransformParameters(svg);
  let dx = 0, dy = 0;

  switch (direction) {
    case "left":
      dx = 3;
      break;
    case "right":
      dx = -3;
      break;
    case "up":
      dy = 3;
      break;
    case "down":
      dy = -3;
      break;
  }

  svg.style.transform = getTransformString(scale, x + dx, y + dy);
};


const zoom = (direction) => {
  const { scale, x, y } = getTransformParameters(svg);
  let dScale = 0.1;
  if (direction == "out") dScale *= -1;
  if (scale == 0.1 && direction == "out") dScale = 0;
  svg.style.transform = getTransformString(scale + dScale, x, y);
};




// Event Listeners

document.getElementById("left-button").onclick = () => pan("left");
document.getElementById("right-button").onclick = () => pan("right");
document.getElementById("up-button").onclick = () => pan("up");
document.getElementById("down-button").onclick = () => pan("down");

document.getElementById("zoom-in-button").onclick = () => zoom("in");
document.getElementById("zoom-out-button").onclick = () => zoom("out");






// ******************** Other Stuff **************************** //

const riigid = document.querySelectorAll("svg path")


const leitavad = ["IS", "LV", "LT", "UA", "HU", "Russian Federation", "France"]
let leitud = [];

// Add click listener
for (let i = 0; i < riigid.length; i++){
    if (leitavad.includes(riigid[i].id) || leitavad.includes(riigid[i].className.baseVal)){
        riigid[i].addEventListener("click", e => {
            e.target.style.fill = "#00FF00";
            if (!leitud.includes(i))
                leitud.push(i);

            if (leitavad.length == leitud.length)
                moveOnEnable();

            console.log(leitud.length);
        })
    }
    else{
        riigid[i].addEventListener("click", e => {
           e.target.style.fill = "#FF0000";
        })
    }
}

function moveOnEnable(){
    const btn = document.querySelector(".moveOn");
    btn.disabled = false;
    btn.onclick = () => location.href = "../rolliValik";
}
