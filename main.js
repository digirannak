let btn = document.querySelector(".neon-button");
const isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;


if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent))
    btn.setAttribute("href", "rannak/noMobiles");

if (isMobile)
    btn.setAttribute("href", "rannak/noMobiles");
